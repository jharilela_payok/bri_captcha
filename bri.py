import cv2
import pytesseract
import re
from matplotlib import pyplot as plt
from PIL import Image
image = cv2.imread('captcha5.png')
pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files/Tesseract-OCR/tesseract.exe'
results = []
regex = '[^0-9]'

img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
results.append(re.sub(regex, '', pytesseract.image_to_string(img).encode('ascii', errors='ignore')))
results.append(re.sub(regex, '', pytesseract.image_to_string(img, config="-c tessedit_char_whitelist=0123456789 ").encode('ascii', errors='ignore')))
print "img ",pytesseract.image_to_string(img)

gray = cv2.threshold(img, 50, 127, cv2.THRESH_TRUNC)[1]
results.append(re.sub(regex, '', pytesseract.image_to_string(gray).encode('ascii', errors='ignore')))
results.append(re.sub(regex, '', pytesseract.image_to_string(gray, config="-c tessedit_char_whitelist=0123456789 ").encode('ascii', errors='ignore')))
print "gray ",pytesseract.image_to_string(gray)


thresh1 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                cv2.THRESH_BINARY, 3, 1)
results.append(re.sub(regex, '', pytesseract.image_to_string(thresh1).encode('ascii', errors='ignore')))
results.append(re.sub(regex, '', pytesseract.image_to_string(thresh1, config="-c tessedit_char_whitelist=0123456789 ").encode('ascii', errors='ignore')))
print "thresh1 ",pytesseract.image_to_string(thresh1)

thresh2 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                cv2.THRESH_BINARY, 11, 2)
results.append(re.sub(regex, '', pytesseract.image_to_string(thresh2).encode('ascii', errors='ignore')))
results.append(re.sub(regex, '', pytesseract.image_to_string(thresh2, config="-c tessedit_char_whitelist=0123456789 ").encode('ascii', errors='ignore')))
print "thresh2 ", pytesseract.image_to_string(thresh2)

print results
print "result ",max(results, key=len)
# cv2.imshow('image '+cap_image,image)
# cv2.imshow('gray '+cap_gray,gray)
# cv2.imshow('ADAPTIVE_THRESH_MEAN_C '+cap_thresh1,gray)
# cv2.imshow('ADAPTIVE_THRESH_GAUSSIAN_C '+cap_thresh2,gray)
f, axarr = plt.subplots(2,2)
axarr[0,0].imshow(image)
axarr[0,1].imshow(gray)
axarr[1,0].imshow(thresh1)
axarr[1,1].imshow(thresh2)
plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()
cap = pytesseract.image_to_string(gray)